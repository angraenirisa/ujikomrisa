<html>
<head>
	<title>CETAK PRINT DATA PENGEMBALIAN</title>
</head>
<body>
 
	<center>
 
		<h2>LAPORAN DATA PENGEMBALIAN</h2>
		
	</center>
 
	
 
	<table border="1" style="width: 100%">
	 <thead>
		<tr>
			   <th>No</th>
                                            <th>Id Peminjaman</th>
                                            <th>Nama Pegawai</th>
                                            <th>Tanggal Pinjam</th>
                                            <th>Tanggal Kembali</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
		</tr>
		 </tr>
                                    <tbody>
                                           <?php
                                           include 'koneksi.php';
                                           $no =1;
                                             $data = mysqli_query($koneksi," SELECT * from peminjaman INNER JOIN pegawai ON peminjaman.id_pegawai=pegawai.id_pegawai where peminjaman.status_peminjaman='dikembalikan' order by peminjaman.id_peminjaman desc");

                                           while($r = mysqli_fetch_array($data)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['id_peminjaman']; ?></td>
                                                  <td><?php echo $r['nama_pegawai']; ?></td>
                                                  <td><?php echo $r['tgl_pinjam']; ?></td>
                                                  <td><?php echo $r['tgl_kembali']; ?></td>
                                                  <td>
                                                    <?php if ($r['status_peminjaman'] == 'dipinjam') {?>
                                                    <form method="post" action="return_barang.php">
                                                        <input type="hidden" name="id_peminjaman" value="<?php echo $r['id_peminjaman']?>">
                                                        <button type="submit" class="btn btn-danger"><?php echo $r['status_peminjaman']; ?></button>
                                                    </form>
                                                    <?php } else{?>
                                                        <button type="submit" class="btn btn-success"><?php echo $r['status_peminjaman']; ?></button>
                                                    <?php } ?>
                                                   </td>
                                                  <td><a href="detail.php?id_peminjaman=<?php echo $r['id_peminjaman']; ?>" class="btn btn-default
                                                  ">DETAIL</a></td>
                                                  
                                                  
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                                </table>
 
	<script>
		window.print();
	</script>
 
</body>
</html>