<?php
session_start();
include "koneksi.php";
if(!isset($_SESSION['username'])){
    echo "<script type=text/javascript>alert('Anda Belum Login');
    window.location.href ='../../../login/index.php';
    </script>";
}
?>

<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminox/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:33:01 GMT -->
<head>
        <meta charset="utf-8" />
        <title>APLIKASI INVENTARIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- C3 charts css -->
        <link href="../plugins/c3/c3.min.css" rel="stylesheet" type="text/css"  />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="dashboard.php" class="logo">
                                <span>
                                    <img src="assets/images/logo.png" alt="" height="25">
                                </span>
                        <i>
                            <img src="assets/images/logo_sm.png" alt="" height="28">
                        </i>
                    </a>
                </div>

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                        

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! John</small> </h5>
                                </div>

                               
                                <!-- item-->
                                <a href="../login/logout.php" class="dropdown-item notify-item">
                                    <i class="mdi mdi-power"></i> <span>Logout</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="mdi mdi-settings"></i> <span>Settings</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="mdi mdi-lock-open"></i> <span>Lock Screen</span>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <i class="mdi mdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li class="hide-phone app-search">
                            <form role="search" class="">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href="#"><i class="fa fa-search"></i></a>
                            </form>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Navigation</li>
                            <li>
                                <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span> Dashboard </span></a>
                                
                            </li>
                            <li>
                                <a href="javascript: void(0);"><i class="fi-air-play"></i> <span>Inventaris</span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="index.php">Data Barang </a></li>
                                     <li><a href="data_ruangan.php">Data Ruangan</a></li>
                                     <li><a href="data_jenis.php">Data Jenis</a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);"><i class="fi-target"></i> <span>Petugas</span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="data_petugas.php">Data Petugas</a></li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);"><i class="fi-briefcase"></i> <span> Pegawai </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="data_pegawai.php">Data Pegawai</a></li>
                                    
                                   
                                </ul>
                            </li>
                            

                            <li>
                                <a href="javascript: void(0);"><i class="fi-bar-graph"></i><span>Level </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="data_level.php">Data Level</a></li>
                                </ul>
                            </li>


                            <li>
                                <a href="javascript: void(0);"><i class="fi-paper"></i> <span> Transaksi </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="peminjaman.php">Peminjaman</a></li>
                                     <li><a href="pengembalian.php">Pengembalian</a></li>
                                   
                                   
                                </ul>
                            </li>

                           <li>
                                <a href="backup.php"><i class="fa fa-window-restore"></i> <span> BackUp </span> </a>
                                
                            </li>

                            <li>
                                <a href="laporan_barang.php"><i class="fi-layers"></i> <span>Generate Laporan <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="laporan_barang.php">Laporan Barang</a></li>
                                    
                                    
                                </ul>
                            </li>

                        </ul>

                    </div>

                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Dashboard</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Adminox</a></li>
                                        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                                        <li class="breadcrumb-item active">Dashboard</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->


                        <div class="row">

                            <div class="col-xl-3 col-sm-6">
                                <div class="card-box widget-box-two widget-two-custom">
                                    <i class="mdi mdi-calendar-text widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics"><font size="4">Tanggal</font></p>
                                        <span class=""><?= date('d-m-Y') ?></span>
                                        <h2 class="font-600"></h2></br></br>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-xl-3 col-sm-6">
                                <div class="card-box widget-box-two widget-two-custom">
                                    <i class="mdi mdi-account-multiple widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics"><font size="4">Total User</font></p>
                                        <span class=""><?php
                                        $koneksi = mysqli_connect("localhost","root","","ujikom");
                                        $table = "petugas";
                                        $sql = "SELECT * FROM $table";
                                        $query = mysqli_query($koneksi,$sql);
                                        $count = mysqli_num_rows($query);
                                        echo '
                                        <span class="count">'.$count; ?></span>
                                        <h2 class="font-600"></h2></br></br>
                                    </div>
                                </div>
                            </div><!-- end col -->

                            <div class="col-xl-3 col-sm-6">
                                <div class="card-box widget-box-two widget-two-custom">
                                    <i class=" mdi mdi-briefcase widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics"><font size="4">Total Barang</font></p>
                                        <span class="">
                                        <?php
                                        $koneksi = mysqli_connect("localhost","root","","ujikom");
                                        $table = "inventaris";
                                        $sql = "SELECT * FROM $table";
                                        $query = mysqli_query($koneksi,$sql);
                                        $count = mysqli_num_rows($query);
                                        echo '
                                       <span class="count">'.$count; ?>
                                       </span>
                                        <h2 class="font-600"></h2></br></br>
                                    </div>
                                </div>
                            </div><!-- end col -->


                            <div class="col-xl-3 col-sm-6">
                                <div class="card-box widget-box-two widget-two-custom">
                                    <i class=" mdi mdi-book-multiple widget-two-icon"></i>
                                    <div class="wigdet-two-content">
                                        <p class="m-0 text-uppercase font-bold font-secondary text-overflow" title="Statistics"><font size="4">Peminjam</font></p>
                                        <span class="">
                                        <?php
                                        $koneksi = mysqli_connect("localhost","root","","ujikom");
                                        $table = "peminjaman";
                                        $sql = "SELECT * FROM $table";
                                        $query = mysqli_query($koneksi,$sql);
                                        $count = mysqli_num_rows($query);
                                        echo '
                                       <span class="count">'.$count; ?>
                                       </span>
                                       </span>
                                        <h2 class="font-600"></h2></br></br>
                                    </div>
                                </div>
                            </div><!-- end col -->
                            
                            </div><!-- end col -->

                        </div>
                        <!-- end row -->


                        
                        <!-- end row -->



                        <!--- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    © Risa Angraeni 2018-2019
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

        <!-- Counter js  -->
        <script src="../plugins/waypoints/jquery.waypoints.min.js"></script>
        <script src="../plugins/counterup/jquery.counterup.min.js"></script>

        <!--C3 Chart-->
        <script type="text/javascript" src="../plugins/d3/d3.min.js"></script>
        <script type="text/javascript" src="../plugins/c3/c3.min.js"></script>

        <!--Echart Chart-->
        <script src="../plugins/echart/echarts-all.js"></script>

        <!-- Dashboard init -->
        <script src="assets/pages/jquery.dashboard.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>

<!-- Mirrored from coderthemes.com/adminox/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:33:41 GMT -->
</html>