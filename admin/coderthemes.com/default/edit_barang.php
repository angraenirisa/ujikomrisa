<?php 
    include "koneksi.php";
    $id_inventaris = $_GET['id_inventaris'];
    $query_mysqli = mysqli_query($koneksi,"SELECT * FROM inventaris WHERE id_inventaris='$id_inventaris'")or die(mysqli_error($koneksi));
    $nomor = 1;
    while($data = mysqli_fetch_array($query_mysqli)){
    ?>

<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminox/default/form-x-editable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:50 GMT -->
<head>
        <meta charset="utf-8" />
        <title>APLIKASI INVENTARIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- X editable -->
        <link href="../plugins/bootstrap-xeditable/css/bootstrap-editable.css" rel="stylesheet" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo">
                                <span>
                                    <img src="assets/images/logo.png" alt="" height="25">
                                </span>
                        <i>
                            <img src="assets/images/logo_sm.png" alt="" height="28">
                        </i>
                    </a>
                </div>

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                        

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! Admin</small> </h5>
                                </div>


                                <!-- item-->
                                <a href="../login/logout.php" class="dropdown-item notify-item">
                                    <i class="mdi mdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li class="hide-phone app-search">
                            <form role="search" class="">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href="#"><i class="fa fa-search"></i></a>
                            </form>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


           



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">
</br></br>
                        <!-- end row -->



                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-20">Edit Data Barang</h4>

                                    <form action="update_barang.php?id_inventaris=<?php echo $id_inventaris;?>" method="post">

                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                        <tr><td>Id Inventaris</td><td><input class="form-control" type="text" name="id_inventaris" value="<?php echo $data['id_inventaris'] ?>" readonly></td> 
                                        </tr>
                                        <tr><td>Nama Barang</td><td><input class="form-control" type="text" name="nama" value="<?php echo $data['nama'] ?>"></td> 
                                        </tr>
                                        <tr><td>Kondisi</td><td><input class="form-control" type="text" name="kondisi" value="<?php echo $data['kondisi'] ?>"></td> 
                                        </tr>
                                        <tr><td>Spesifikasi</td><td><input class="form-control" type="text" name="spesifikasi" value="<?php echo $data['spesifikasi'] ?>">
                                        </td> 
                                        </tr>
                                       <tr><td>Keterangan</td><td><input class="form-control" type="text" name="keterangan" value="<?php echo $data['keterangan'] ?>"></td> 
                                        </tr>
                                        <tr><td>Jumlah</td><td><input class="form-control" type="text" name="jumlah" value="<?php echo $data['jumlah'] ?>"></td> 
                                        </tr>


                                        <?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
                                                      $jsArray = "var id_jenis = new Array();\n";
                                                      ?> 
                                                        <td>Id Jenis</td>
                                                        <td>
                                                            <select class="form-control" type="text" class="form-control" name="id_jenis">
                                                            <option selected="selected"><?php echo $data['id_jenis'];?>
                                                              <?php 
                                                              while($row = mysqli_fetch_array($result)){
                                                                echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                                $jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                              }
                                                              ?>


                                        
                                        <tr>
                                            <td>Tanggal Register</td><td>
                                            <input class="form-control" type="date" name="tgl_register" id="tgl_register" value="<?php echo $data['tgl_register'];?>" readonly></td>
                                        </tr>
                                        
                                        <?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
                                                      $jsArray = "var id_ruang = new Array();\n";
                                                      ?> 
                                                        <td>Id Ruang</td>
                                                        <td>
                                                            <select class="form-control" type="text" class="form-control" name="id_ruang">
                                                            <option selected="selected"><?php echo $data['id_ruang'];?>
                                                              <?php 
                                                              while($row = mysqli_fetch_array($result)){
                                                                echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                                $jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                              }
                                                              ?>

                                        <tr><td>Kode Inventaris</td><td><input class="form-control" type="text" name="kode_inventaris" value="<?php echo $data['kode_inventaris'] ?>" readonly></td> 
                                        </tr>
                                        
                                        <?php
                                                      include "koneksi.php";
                                                      $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
                                                      $jsArray = "var id_petugas = new Array();\n";
                                                      ?> 
                                                        <td>Id Petugas</td>
                                                        <td>
                                                            <select class="form-control" type="text" class="form-control" name="id_petugas">
                                                            <option selected="selected"><?php echo $data['id_petugas'];?>
                                                              <?php 
                                                              while($row = mysqli_fetch_array($result)){
                                                                echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
                                                                $jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
                                                              }
                                                              ?>

                                        <tr><td>Sumber</td><td><input class="form-control" type="text" name="sumber" value="<?php echo $data['sumber'] ?>"></td> 
                                        </tr>


                                        </tbody>
                                    </table>
                                    <button class="btn btn-primary" name="simpan" type="submit" id="simpan">Simpan</button>

                                     <a class="btn btn-danger" href="index.php" type="cancel" >Cancel</a>
                                </div>
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    © Risa Angraeni 2018-2019
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

        <!-- Xeditable -->
        <script src="../plugins/moment/moment.js" type="text/javascript"></script>
        <script src="../plugins/bootstrap-xeditable/js/bootstrap-editable.min.js" type="text/javascript"></script>
        <script src="assets/pages/jquery.xeditable.init.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>

<!-- Mirrored from coderthemes.com/adminox/default/form-x-editable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:51 GMT -->
</html>
<?php } ?>