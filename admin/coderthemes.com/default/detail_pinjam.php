 <?php
session_start();
include "koneksi.php";
if(!isset($_SESSION['username'])){
    echo "<script type=text/javascript>alert('Anda Belum Login');
    window.location.href ='../../../login/index.php';
    </script>";
}
?>
<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminox/default/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:53 GMT -->
<head>
        <meta charset="utf-8" />
        <title>APLIKASI INVENTARIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- DataTables -->
        <link href="../plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="../plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="../plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="" class="logo">
                                <span>
                                    <img src="assets/images/logo.png" alt="" height="25">
                                </span>
                        <i>
                            <img src="assets/images/logo_sm.png" alt="" height="28">
                        </i>
                    </a>
                </div>

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                               
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! Admin</small> </h5>
                                </div>


                                <!-- item-->
                                <a href="../login/logout.php" class="dropdown-item notify-item">
                                    <i class="mdi mdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li class="hide-phone app-search">
                            <form role="search" class="">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href="#"><i class="fa fa-search"></i></a>
                            </form>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Navigation</li>
                            <li>
                                <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span> Dashboard </span> <span class="menu-arrow"></span></a>
                                
                            </li>
                            <li>
                                <a href="javascript: void(0);"><i class="fi-air-play"></i> <span>Inventaris</span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="index.php">Data Barang </a></li>
                                     <li><a href="data_ruangan.php">Data Ruangan</a></li>
                                     <li><a href="data_jenis.php">Data Jenis</a></li>
                                    
                                </ul>
                            </li>
                            <li>
                                <a href="javascript: void(0);"><i class="fi-target"></i> <span>Petugas</span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="data_petugas.php">Data Petugas</a></li>
                                    
                                </ul>
                            </li>

                            <li>
                                <a href="javascript: void(0);"><i class="fi-briefcase"></i> <span> Pegawai </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="data_pegawai.php">Data Pegawai</a></li>
                                    
                                   
                                </ul>
                            </li>
                            

                            <li>
                                <a href="javascript: void(0);"><i class="fi-bar-graph"></i><span>Level </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="data_level.php">Data Level</a></li>
                                </ul>
                            </li>


                            <li>
                                <a href="javascript: void(0);"><i class="fi-paper"></i> <span> Transaksi </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="peminjaman.php">Peminjaman</a></li>
                                     <li><a href="pengembalian.php">Pengembalian</a></li>
                                   
                                   
                                </ul>
                            </li>

                           <li>
                                <a href="backup.php"><i class="fa fa-window-restore"></i> <span> BackUp </span></a>
                                
                            </li>

                            <li>
                                <a href="laporan_barang.php"><i class="fi-layers"></i> <span>Generate Laporan <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="laporan_barang.php">Laporan Barang</a></li>
                                    <li><a href="laporan_transaksi.php">Laporan Transaksi Peminjaman</a></li>
                                    <li><a href="laporan_transaksi.php">Laporan Transaksi Pengembalian</a></li>
                                    
                                </ul>
                            </li>


                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Peminjaman</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Adminox</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->







                                      <div class="card-box table-responsive">



<table id="datatable-buttons" class="table table-bordered">
                            <div class="row">
                            <div class="col-12">
 <p><button type="button" class="btn btn-inverse waves-light waves-effect w-md" data-toggle="modal" data-target="#myModal" ><i class="fa fa-plus" aria-hidden="true" style="color: white;"> Pinjam</i></a></button><p>
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Barang</th>
                                           
                                           
                                        </tr>
                                        </thead>


                                        <tbody>
                                           <?php
                                           include 'koneksi.php';
                                           $no =1;
                                           $id=$_GET['id_peminjaman'];
                                           $select=mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN detail_pinjam ON inventaris.id_inventaris=detail_pinjam.id_inventaris where detail_pinjam.id_peminjaman='$id' order by inventaris.id_inventaris desc");                                       
                                           while($r = mysqli_fetch_array($select)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['nama']; ?></td>
                                                  <td><?php echo $r['jumlah']; ?>buah</td>
                                                  
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                                    </table>
        
	</div>
        </div>
		
		
                                 
     <!-- MODAL -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Peminjam</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="proses_detail_pinjam.php">
        <div class="form-group">
          <label>Nama Barang</label>
          <input type="hidden"  name="id_peminjaman" value="<?php echo $_GET['id_peminjaman']?>" class="form-control">
          <select class="form-control"  name="id_inventaris"  required="">
              <option value="">--- Silahkan Cari ---</option>

              <?php
              include_once "koneksi.php";
              $tampil=mysqli_query($koneksi,"SELECT * FROM inventaris WHERE jumlah>0 ORDER BY id_inventaris desc");
              while($r=mysqli_fetch_array($tampil)){
                ?>
                <option value="<?php echo $r['id_inventaris']?>"> <?php echo $r['nama'] ?> | <?php echo "(stock : ".$r['jumlah'].")" ?></option>
                <?php
            }
            ?>

        </select>
        </div>
        <div class="form-group">
          <label>Jumlah Barang</label>
          <input type="text"  name="jumlah" class="form-control" 
           onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
           onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <input type="submit" name="pbarang" Value="Tambah" class="btn btn-primary">
        </div>
        </form>
      </div>
    </div>
  </div>
 
</div>
		</div> 
                        </div>
                    </table>
                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    © Risa Angraeni 2018-2019
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

        <!-- Required datatable js -->
        <script src="../plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../plugins/datatables/jszip.min.js"></script>
        <script src="../plugins/datatables/pdfmake.min.js"></script>
        <script src="../plugins/datatabwles/vfs_fonts.js"></script>
        <script src="../plugins/datatables/buttons.html5.min.js"></script>
        <script src="../plugins/datatables/buttons.print.min.js"></script>
        <script src="../plugins/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="../plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: true,
                    buttons: ['copy', 'excel', 'pdf', 'colvis']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>

<!-- Mirrored from coderthemes.com/adminox/default/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:56 GMT -->
</html>

            
<script type="text/javascript" language=JavaScript>
function inputDigitsOnly(e) {
 var chrTyped, chrCode=0, evt=e?e:event;
 if (evt.charCode!=null)     chrCode = evt.charCode;
 else if (evt.which!=null)   chrCode = evt.which;
 else if (evt.keyCode!=null) chrCode = evt.keyCode;

 if (chrCode==0) chrTyped = 'SPECIAL KEY';
 else chrTyped = String.fromCharCode(chrCode);

 //[test only:] display chrTyped on the status bar 
 self.status='inputDigitsOnly: chrTyped = '+chrTyped;

 //Digits, special keys & backspace [\b] work as usual:
 if (chrTyped.match(/\d|[\b]|SPECIAL/)) return true;
 if (evt.altKey || evt.ctrlKey || chrCode<28) return true;

 //Any other input? Prevent the default response:
 if (evt.preventDefault) evt.preventDefault();
 evt.returnValue=false;
 return false;
}

function addEventHandler(elem,eventType,handler) {
 if (elem.addEventListener) elem.addEventListener (eventType,handler,false);
 else if (elem.attachEvent) elem.attachEvent ('on'+eventType,handler); 
 else return 0;
 return 1;
}

// onload: Call the init() function to add event handlers!
function init() {
 addEventHandler(self.document.f2.elements[0],'keypress',inputDigitsOnly);
 addEventHandler(self.document.f2.elements[1],'keypress',inputDigitsOnly);
}

</script>