<!DOCTYPE html>
<html>
<head>
  <title>Inventaris SMK</title>
</head>
<body>
  <style type="text/css">
  body{
    font-family: sans-serif;
  }
  table{
    margin: 20px auto;
    border-collapse: collapse;
  }
  table th,
  table td{
    border: 1px solid #3c3c3c;
    padding: 3px 8px;

  }
  a{
    background: blue;
    color: #fff;
    padding: 8px 10px;
    text-decoration: none;
    border-radius: 2px;
  }
  </style>

  <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=Data Level.xls");
  ?>

  <center>
    <h1>Data Level</h1>
  </center>

  <table border="1">
   <thead>
    <tr>
       <th>No</th>
        <th>Id Level</th>
        <th>Nama Level</th>
                                          
    </tr>
     </tr>
                                    </thead>
                                    <tbody>
                                    <?php
            include 'koneksi.php';
            $no=1;
            $select = mysqli_query($koneksi,"SELECT * FROM level ORDER BY id_level desc");
            while ($data = mysqli_fetch_array($select))
            {
                ?>

                                        <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $data['id_level']; ?></td>
                    <td><?php echo $data['nama_level']; ?></td>
                    

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                 
</body>
</html> 