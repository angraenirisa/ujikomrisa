<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminox/default/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:38 GMT -->
<head>
        <meta charset="utf-8" />
        <title>APLIKASI INVENTARIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="assets/images/favicon.ico">

        <!-- App css -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo">
                                <span>
                                    <img src="assets/images/logo.png" alt="" height="25">
                                </span>
                        <i>
                            <img src="assets/images/logo_sm.png" alt="" height="28">
                        </i>
                    </a>
                </div>

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                       
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! Admin</small> </h5>
                                </div>


                                <!-- item-->
                                <a href="../login/logout.php" class="dropdown-item notify-item">
                                    <i class="mdi mdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li class="hide-phone app-search">
                            <form role="search" class="">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href="#"><i class="fa fa-search"></i></a>
                            </form>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container"></br></br>

                        <!-- end row -->

						<form action="simpan_inventaris.php" method="post" enctype="multipart/form-data" name="form1" id="form1">  
                        <div class="row">
                            <div class="col-12">
                                <div class="card-box">

                                    <div class="row">
                                        <div class="col-12">
                                            <div class="p-20">
                                                <form class="form-horizontal" role="form">
													
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Nama Barang</label>
                                                        <div class="col-10">
                                                            <input type="text" required="" class="form-control" name="nama" value="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label" for="example-email">Kondisi</label>
                                                        <div class="col-10">
                                                            <input type="text" required="" class="form-control" name="kondisi" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label" for="example-email">Spesifikasi</label>
                                                        <div class="col-10">
                                                            <input type="text" required="" class="form-control" name="spesifikasi" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Keterangan</label>
                                                        <div class="col-10">
                                                            <input type="text" required="" class="form-control" name="keterangan" value="">
                                                        </div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Jumlah</label>
                                                        <div class="col-10">
                                                            <input type="number" required="" class="form-control" name="jumlah" placeholder="">
                                                        </div>
                                                    </div>
													<?php
													  include "koneksi.php";
													  $result = mysqli_query($koneksi,"select * from jenis order by id_jenis asc ");
													  $jsArray = "var id_jenis = new Array();\n";
													  ?>
													
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Id Jenis</label>
                                                        <div class="col-10">
                                                            <select class="form-control" type="text" class="form-control" name="id_jenis">
															  <?php 
															  while($row = mysqli_fetch_array($result)){
																echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
																$jsArray .= "id_jenis['". $row['id_jenis']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
															  }
															  ?>
															 </select> 
														</div>
                                                    </div>

                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Tanggal Register</label>
                                                        <div class="col-10">
                                                            <input id="tgl_register" type="text"  class="form-control" name="tgl_register" value="<?php $tgl = Date('Y-m-d');
                                                            echo $tgl; ?>" readonly>
														</div>
                                                    </div>
													
													<?php
													  include "koneksi.php";
													  $result = mysqli_query($koneksi,"select * from ruang order by id_ruang asc ");
													  $jsArray = "var id_ruang = new Array();\n";
													  ?>
													<div class="form-group row">
                                                        <label class="col-2 col-form-label">Id Ruang</label>
                                                        <div class="col-10">
                                                            <select class="form-control" type="text" class="form-control" name="id_ruang">
															
															  <?php 
																  while($row = mysqli_fetch_array($result)){
																	echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
																	$jsArray .= "id_ruang['". $row['id_ruang']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
																  }
																  ?>
															 </select> 
														</div>
                                                    </div>
                                                    
													<div class="form-group row">
                                                        <label class="col-2 col-form-label">Kode Inventaris</label>
                                                        <div class="col-10">
                                                        <?php
                                                        $koneksi = mysqli_connect("localhost","root","","ujikom");
                                                        $cari_kd = mysqli_query($koneksi,"select max(kode_inventaris) as kode from inventaris");
                                                        //besar atau kode yang baru masuk
                                                        $tm_cari=mysqli_fetch_array($cari_kd);
                                                        $kode=substr($tm_cari['kode'],1,4);
                                                        $tambah=$kode+1;
                                                        if($tambah<10){
                                                            $kode_inventaris="V000".$tambah;
                                                        }else{
                                                            $kode_inventaris="V00".$tambah;
                                                        }
                                                        ?>
                                                            <input type="text" value="<?php echo $kode_inventaris; ?>" class="form-control" name="kode_inventaris" placeholder="kode inventaris" readonly>
                                                        </div>
                                                    </div>
													
													<?php
													  include "koneksi.php";
													  $result = mysqli_query($koneksi,"select * from petugas order by id_petugas asc ");
													  $jsArray = "var id_petugas = new Array();\n";
													  ?>
													<div class="form-group row">
                                                        <label class="col-2 col-form-label">Id Petugas</label>
                                                        <div class="col-10">
                                                            <select class="form-control" type="text" class="form-control" name="id_petugas">
															  <?php 
															  while($row = mysqli_fetch_array($result)){
																echo "<option value='$row[0].$row[1]'>$row[0]. $row[1]</option>";
																$jsArray .= "id_petugas['". $row['id_petugas']. "'] = {satu:'" . addslashes($row['no']) . "'};\n";
															  }
															  ?>
															 </select> 
														</div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label" for="example-email">Sumber</label>
                                                        <div class="col-10">
                                                            <input type="text" required="" class="form-control" name="sumber" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                    
													<button class="btn btn-primary" name="simpan" type="submit" id="simpan">
                                                    Simpan</button>		

                                                    <a class="btn btn-danger" href="index.php" type="cancel" >Cancel</a>																
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                    <!-- end row -->

                                </div> <!-- end card-box -->
                            </div><!-- end col -->
                        </div>
                        <!-- end row -->


                        

                        
                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    © Risa Angraeni 2018-2019
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/metisMenu.min.js"></script>
        <script src="assets/js/waves.js"></script>
        <script src="assets/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="assets/js/jquery.core.js"></script>
        <script src="assets/js/jquery.app.js"></script>

    </body>

<!-- Mirrored from coderthemes.com/adminox/default/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:38 GMT -->
</html>