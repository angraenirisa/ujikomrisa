<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminox/default/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:39:09 GMT -->
<head>
        <meta charset="utf-8" />
        <title>APLIKASI INVENTARIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="../admin/coderthemes.com/default/assets/images/favicon.ico">

        <!-- App css -->
        <link href="../admin/coderthemes.com/default/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/default/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/default/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/default/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="../admin/coderthemes.com/default/assets/js/modernizr.min.js"></script>
        <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />

    </head>


    <body class="bg-accpunt-pages" style="background: linear-gradient(to right, #00dbde, #2b2ed4);">
        

        <!-- HOME -->
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="wrapper-page">

                            <div class="account-pages">
                                <div class="account-box">
                                    <div class="account-logo-box">
                                        <h2 class="text-uppercase text-center">
                                            <a href="index.html" class="text-success">
                                                <h2 style="font-size:100px; clear:both;"><span class="fa fa-lock"></span></h2>
                                                <h3>Silahkan Login</h3>
                                            </a>
                                        </h2>
                                       
                                    </div>
                                    <div class="account-content">
                                        <form class="form-horizontal" action="cek_login.php" method="post">

                                            <div class="form-group m-b-20 row">
                                                <div class="col-12">
                                                    <label>Username</label>
                                                    <input class="form-control" type="text" name="username"  placeholder="Username...">
                                                </div>
                                            </div>

                                            <div class="form-group row m-b-20">
                                                <div class="col-12"> 
                                                    <label for="password">Password</label>
                                                    <input class="form-control" type="password" name="password" placeholder="Enter your password">
                                                </div>
                                            </div>


                                            <div class="form-group row text-center m-t-10">
                                                <div class="col-12">
                                                    <button class="btn btn-md btn-block btn-custom waves-effect waves-light" type="submit">Sign In</button>
                                                </div>
                                            </div>

                                        </form>

                                        

                                        <div class="row m-t-50">
                                            <div class="col-sm-12 text-center">
                                                 <a href="forget.php" class="text-dark m-l-5"><b class="text-muted">Forgot your password?</b></a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- end card-box-->


                        </div>
                        <!-- end wrapper -->

                    </div>
                </div>
            </div>
          </section>
          <!-- END HOME -->



        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="../admin/coderthemes.com/default/assets/js/jquery.min.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="../admin/coderthemes.com/default/assets/js/bootstrap.min.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/metisMenu.min.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/waves.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="admin/coderthemes.com/default/assets/js/jquery.core.js"></script>
        <script src="admin/coderthemes.com/default/assets/js/jquery.app.js"></script>

    </body>

<!-- Mirrored from coderthemes.com/adminox/default/page-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:39:09 GMT -->
</html>