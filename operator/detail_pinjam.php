 <?php
session_start();
include "../koneksi.php";
if(!isset($_SESSION['username'])){
    echo "<script type=text/javascript>alert('Anda Belum Login');
    window.location.href ='../login/index.php';
    </script>";
}
?>
<!DOCTYPE html>
<html>
    
<!-- Mirrored from coderthemes.com/adminox/default/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:53 GMT -->
<head>
        <meta charset="utf-8" />
        <title>APLIKASI INVENTARIS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="../admin/coderthemes.com/default/assets/images/favicon.ico">

        <!-- DataTables -->
        <link href="../admin/coderthemes.com/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
        <!-- Responsive datatable examples -->
        <link href="../admin/coderthemes.com/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="../admin/coderthemes.com/default/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/default/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/default/assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="../admin/coderthemes.com/default/assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="../admin/coderthemes.com/default/assets/js/modernizr.min.js"></script>

    </head>


    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="dashboard.php" class="logo">
                                <span>
                                    <img src="assets/images/logo.png" alt="" height="25">
                                </span>
                        <i>
                            <img src="assets/images/logo_sm.png" alt="" height="28">
                        </i>
                    </a>
                </div>

                <nav class="navbar-custom">

                    <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle arrow-none waves-light waves-effect" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <i class="dripicons-bell noti-icon"></i>
                                <span class="badge badge-pink noti-icon-badge">4</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-lg" aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5><span class="badge badge-danger float-right">5</span>Notification</h5>
                                </div>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-success"><i class="icon-bubble"></i></div>
                                    <p class="notify-details">Robert S. Taylor commented on Admin<small class="text-muted">1 min ago</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-info"><i class="icon-user"></i></div>
                                    <p class="notify-details">New user registered.<small class="text-muted">1 min ago</small></p>
                                </a>

                                <!-- item-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item">
                                    <div class="notify-icon bg-danger"><i class="icon-like"></i></div>
                                    <p class="notify-details">Carlos Crouch liked <b>Admin</b><small class="text-muted">1 min ago</small></p>
                                </a>

                                <!-- All-->
                                <a href="javascript:void(0);" class="dropdown-item notify-item notify-all">
                                    View All
                                </a>

                            </div>
                        </li>

                        <li class="list-inline-item dropdown notification-list">
                            <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                               aria-haspopup="false" aria-expanded="false">
                                <img src="assets/images/users/avatar-1.jpg" alt="user" class="rounded-circle">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! Admin</small> </h5>
                                </div>


                                <!-- item-->
                                <a href="../login/logout.php" class="dropdown-item notify-item">
                                    <i class="mdi mdi-power"></i> <span>Logout</span>
                                </a>

                            </div>
                        </li>

                    </ul>

                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                                <i class="dripicons-menu"></i>
                            </button>
                        </li>
                        <li class="hide-phone app-search">
                            <form role="search" class="">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href="#"><i class="fa fa-search"></i></a>
                            </form>
                        </li>
                    </ul>

                </nav>

            </div>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="slimscroll-menu" id="remove-scroll">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <!-- Left Menu Start -->
                        <ul class="metismenu" id="side-menu">
                            <li class="menu-title">Navigation</li>
                            <li>
                                <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span> Dashboard </span></a>
                                
                            </li>
                            <li>
                                <a href="javascript: void(0);"><i class="fi-paper"></i> <span> Transaksi </span> <span class="menu-arrow"></span></a>
                                <ul class="nav-second-level" aria-expanded="false">
                                    <li><a href="index.php">Peminjaman</a></li>
                                     <li><a href="pengembalian.php">Pengembalian</a></li>
                                    
                                   
                                </ul>
                            </li>
                        </ul>

                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->



            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <h4 class="page-title float-left">Peminjaman</h4>

                                    <ol class="breadcrumb float-right">
                                        <li class="breadcrumb-item"><a href="#">Adminox</a></li>
                                        <li class="breadcrumb-item"><a href="#">Tables</a></li>
                                        <li class="breadcrumb-item active">Datatable</li>
                                    </ol>

                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->
<div class="card-box table-responsive">
<table id="datatable-buttons" class="table table-bordered">
                            <div class="row">
                            <div class="col-12">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah Barang</th>
                                           
                                           
                                        </tr>
                                        </thead>


                                        <tbody>
                                           <?php
                                            include '../admin/coderthemes.com/default/koneksi.php';
                                           $no =1;
                                           $id=$_GET['id_peminjaman'];
                                           $select=mysqli_query($koneksi,"SELECT * FROM inventaris INNER JOIN detail_pinjam ON inventaris.id_inventaris=detail_pinjam.id_inventaris where detail_pinjam.id_peminjaman='$id' order by inventaris.id_inventaris desc");                                       
                                           while($r = mysqli_fetch_array($select)){
                                              ?>

                                              <tr>
                                                  <th scope="row"><?php echo $no++;?></th>
                                                  <td><?php echo $r['nama']; ?></td>
                                                  <td><?php echo $r['jumlah']; ?>buah</td>
                                                  
                                              </tr>
                                              <?php 
                                          }
                                          ?>
                                        </tbody>
                                    </table>
         <button type="button" class="btn btn-inverse waves-light waves-effect w-md" data-toggle="modal" data-target="#myModal" ><i class="fa fa-plus" aria-hidden="true" style="color: white;"> Pinjam</i></a></button>
    </div>
        </div>
        
        
                                 
     <!-- MODAL -->
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Peminjam</h4>
        </div>
        <div class="modal-body">
        <form method="POST" action="proses_detail_pinjam.php">
        <div class="form-group">
          <label>Nama Barang</label>
          <input type="hidden"  name="id_peminjaman" value="<?php echo $_GET['id_peminjaman']?>" class="form-control">
          <select class="form-control"  name="id_inventaris"  required="">
              <option value="">--- Silahkan Cari ---</option>

              <?php
              include_once "koneksi.php";
              $tampil=mysqli_query($koneksi,"SELECT * FROM inventaris WHERE jumlah>0 ORDER BY id_inventaris desc");
              while($r=mysqli_fetch_array($tampil)){
                ?>
                <option value="<?php echo $r['id_inventaris']?>"> <?php echo $r['nama'] ?> | <?php echo "(stock : ".$r['jumlah'].")" ?></option>
                <?php
            }
            ?>

        </select>
        </div>
        <div class="form-group">
          <label>Jumlah Barang</label>
          <input type="text"  name="jumlah" class="form-control" 
           onkeypress="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')"
           onkeyup   ="if(this.value.match(/\D/)) this.value=this.value.replace(/\D/g,'')" required>
        </div>
        <div class="modal-footer">
         <input type="submit" name="pinjam2" class="btn btn-inverse btn-sm" value="Cetak">
          <input type="submit" name="pbarang" Value="Tambah" class="btn btn-primary">
        </div>
        </form>
      </div>
    </div>
  </div>
 
</div>
        </div> 
                        </div>
                    </table>

                        <!-- end row -->


                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    © Risa Angraeni 2018-2019
                </footer>

            </div>


            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="../admin/coderthemes.com/default/assets/js/jquery.min.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/popper.min.js"></script><!-- Popper for Bootstrap -->
        <script src="../admin/coderthemes.com/default/assets/js/bootstrap.min.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/metisMenu.min.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/waves.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/jquery.slimscroll.js"></script>

        <!-- Required datatable js -->
        <script src="../admin/coderthemes.com/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <!-- Buttons examples -->
        <script src="../admin/coderthemes.com/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/buttons.bootstrap4.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/jszip.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/pdfmake.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatabwles/vfs_fonts.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/buttons.html5.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/buttons.print.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/buttons.colVis.min.js"></script>
        <!-- Responsive examples -->
        <script src="../admin/coderthemes.com/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="../admin/coderthemes.com/plugins/datatables/responsive.bootstrap4.min.js"></script>

        <!-- App js -->
        <script src="../admin/coderthemes.com/default/assets/js/jquery.core.js"></script>
        <script src="../admin/coderthemes.com/default/assets/js/jquery.app.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#datatable').DataTable();

                //Buttons examples
                var table = $('#datatable-buttons').DataTable({
                    lengthChange: true,
                    buttons: ['copy', 'excel', 'pdf', 'colvis']
                });

                table.buttons().container()
                        .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
            } );

        </script>

    </body>

<!-- Mirrored from coderthemes.com/adminox/default/tables-datatable.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 10 Jan 2019 02:38:56 GMT -->
</html>