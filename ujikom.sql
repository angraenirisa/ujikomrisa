-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2019 at 03:01 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujikom`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pinjam`
--

CREATE TABLE `detail_pinjam` (
  `id_detail_pinjam` int(20) NOT NULL,
  `id_inventaris` int(11) NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_peminjaman` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_pinjam`
--

INSERT INTO `detail_pinjam` (`id_detail_pinjam`, `id_inventaris`, `jumlah`, `id_peminjaman`) VALUES
(39, 61, 1, 46),
(40, 53, 1, 46),
(41, 58, 1, 47),
(42, 56, 1, 47),
(43, 61, 10, 48),
(44, 56, 6, 48),
(45, 56, 6, 48),
(46, 55, 1, 48),
(47, 55, 2, 48),
(48, 55, 12, 48),
(49, 53, 8, 49),
(50, 53, 8, 50),
(51, 53, 10, 51),
(52, 58, 31, 51),
(53, 51, 10000, 51),
(54, 57, 22, 51),
(55, 54, 10, 51),
(56, 63, 5, 52);

--
-- Triggers `detail_pinjam`
--
DELIMITER $$
CREATE TRIGGER `kembali` BEFORE UPDATE ON `detail_pinjam` FOR EACH ROW UPDATE inventaris SET jumlah = jumlah+NEW.jumlah WHERE id_inventaris = new.id_inventaris
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `pengurangan_stok` AFTER INSERT ON `detail_pinjam` FOR EACH ROW UPDATE inventaris SET jumlah = jumlah - NEW.jumlah WHERE id_inventaris = new.id_inventaris
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `inventaris`
--

CREATE TABLE `inventaris` (
  `id_inventaris` int(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `kondisi` text NOT NULL,
  `spesifikasi` text NOT NULL,
  `keterangan` text NOT NULL,
  `jumlah` int(20) NOT NULL,
  `id_jenis` int(20) NOT NULL,
  `tgl_register` date NOT NULL,
  `id_ruang` int(20) NOT NULL,
  `kode_inventaris` varchar(30) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `sumber` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventaris`
--

INSERT INTO `inventaris` (`id_inventaris`, `nama`, `kondisi`, `spesifikasi`, `keterangan`, `jumlah`, `id_jenis`, `tgl_register`, `id_ruang`, `kode_inventaris`, `id_petugas`, `sumber`) VALUES
(49, 'LAPTOP', 'BAIK', 'corei3', 'ACER', 80, 1, '2019-02-21', 1, 'V0001', 1, 'SEKOLAH'),
(50, 'KURSI', 'BAIK', '', 'BARU', 25, 2, '2019-02-21', 1, 'V0002', 1, ''),
(51, 'MEJA', 'BAIK', '', 'BARU', 85, 2, '2019-02-21', 2, 'V0003', 1, ''),
(52, 'PAPAN TULIS', 'BAIK', '', 'ADA', 10, 2, '2019-02-21', 3, 'V0004', 1, ''),
(53, 'PROYEKTOR', 'BAIK', '', 'ADA', 8, 1, '2019-02-21', 1, 'V0005', 1, ''),
(54, 'JAM DINDING', 'BAIK', 'Q & Q', 'BARU', 30, 2, '2019-02-21', 6, 'V0006', 1, ''),
(55, 'WIFI', 'BAIK', '', 'ADA', 12, 1, '2019-02-21', 1, 'V0007', 1, ''),
(57, 'FOTO PRESIDEN', 'BAIK', 'KACA', 'ADA', 20, 2, '2019-02-21', 2, 'V0009', 1, 'PT.SUMBER'),
(58, 'LEMARI', 'RUSAK RINGAN', 'OLYMPIC', 'ADA', 30, 2, '2019-02-21', 2, 'V0010', 1, 'PT.SENTOSA'),
(61, 'HARDDISK', 'BARU', '1TB', 'ADA', 10, 1, '2019-04-06', 1, 'V0013', 1, 'SEKOLAH'),
(63, 'PEMADAM API', 'BARU', 'AIR', 'TERDAPAT DI LAB 2', 10, 2, '2019-04-06', 2, 'V0014', 1, 'PT.SUMBER');

-- --------------------------------------------------------

--
-- Table structure for table `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(20) NOT NULL,
  `nama_jenis` varchar(30) NOT NULL,
  `kode_jenis` varchar(20) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `nama_jenis`, `kode_jenis`, `keterangan`) VALUES
(1, 'ELEKTRONIK', '1', 'BAIK'),
(2, 'NON ELEKTRONIK', '2', 'BAIK'),
(4, 'Kendaraan', 'K1', 'BAIK'),
(6, 'ORGANIK', 'F0002', 'BAIK'),
(7, 'RAM                           ', 'F0002', 'ADA');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(20) NOT NULL,
  `nama_level` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `nama_level`) VALUES
(1, 'admin'),
(2, 'operator'),
(3, 'peminjam'),
(4, 'Pemakai'),
(5, 'Pengguna'),
(6, 'Pengawas');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(20) NOT NULL,
  `nama_pegawai` varchar(30) NOT NULL,
  `nip` int(20) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `nama_pegawai`, `nip`, `alamat`) VALUES
(1, 'ENJE ROSADI', 929299, 'Bogor'),
(8, 'DIDING', 9876543, 'Bogor'),
(9, 'CUNCUN', 12345678, 'ciomas'),
(10, 'ARI', 88776765, 'ciomas'),
(11, 'ADEYANSYAH', 95695996, 'Bogor'),
(12, 'YUDI', 12345465, 'Bogor');

-- --------------------------------------------------------

--
-- Stand-in structure for view `peminjam`
-- (See below for the actual view)
--
CREATE TABLE `peminjam` (
`id_inventaris` int(20)
,`id_peminjaman` int(20)
,`nama_pegawai` varchar(30)
,`nama` varchar(30)
,`nama_ruang` varchar(30)
,`jumlah` int(20)
,`status_peminjaman` enum('dipinjam','dikembalikan')
,`tgl_pinjam` date
,`tgl_kembali` date
);

-- --------------------------------------------------------

--
-- Table structure for table `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(20) NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_kembali` date DEFAULT NULL,
  `status_peminjaman` enum('dipinjam','dikembalikan') NOT NULL,
  `id_pegawai` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `tgl_pinjam`, `tgl_kembali`, `status_peminjaman`, `id_pegawai`) VALUES
(46, '2019-04-06', '2019-04-06', 'dikembalikan', 11),
(47, '2019-04-06', '2019-04-06', 'dikembalikan', 10),
(48, '2019-04-06', '2019-04-06', 'dikembalikan', 11),
(49, '2019-04-06', '2019-04-06', 'dikembalikan', 12),
(50, '2019-04-06', '2019-04-06', 'dikembalikan', 12),
(51, '2019-04-06', '2019-04-06', 'dikembalikan', 12),
(52, '2019-04-06', NULL, 'dipinjam', 12),
(53, '2019-04-06', '2019-04-06', 'dikembalikan', 10);

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` int(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL,
  `nama_petugas` varchar(30) NOT NULL,
  `id_level` int(11) NOT NULL,
  `baned` enum('N','Y') NOT NULL,
  `logintime` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id_petugas`, `username`, `email`, `password`, `nama_petugas`, `id_level`, `baned`, `logintime`) VALUES
(1, 'administrator', 'nadyanovaa6@gmail.com', 'administrator', 'admin', 1, 'N', 0),
(2, 'operator', 'nadyanovaa6@gmail.com', 'operator', 'operator', 2, 'N', 0),
(3, 'peminjam', 'risaangraeni090@gmail.com', 'peminjam', 'pinjam', 3, 'N', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `id_ruang` int(20) NOT NULL,
  `nama_ruang` varchar(30) NOT NULL,
  `kode_ruang` varchar(20) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`id_ruang`, `nama_ruang`, `kode_ruang`, `keterangan`) VALUES
(1, 'Lab RPL 1', 'R1', 'BAIK'),
(2, 'Lab RPL 2', 'R2', 'BAIK'),
(3, 'Lab ANM', 'A1', 'BAIK'),
(4, 'BENGKEL PENGELASAN', 'P1', 'BAIK'),
(6, 'BENGKEL TKR 1', 'T1', 'BARU'),
(7, 'BENGKEL TKR 2', 'T2', 'BARU'),
(9, 'XII RPL 1', 'R0003', 'BAIK'),
(10, 'XII RPL 2', 'R0003', 'BAIK'),
(11, 'LAB ANIMASI 1', 'R0003', 'BAIK');

-- --------------------------------------------------------

--
-- Structure for view `peminjam`
--
DROP TABLE IF EXISTS `peminjam`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `peminjam`  AS  select `c`.`id_inventaris` AS `id_inventaris`,`d`.`id_peminjaman` AS `id_peminjaman`,`a`.`nama_pegawai` AS `nama_pegawai`,`c`.`nama` AS `nama`,`e`.`nama_ruang` AS `nama_ruang`,`b`.`jumlah` AS `jumlah`,`d`.`status_peminjaman` AS `status_peminjaman`,`d`.`tgl_pinjam` AS `tgl_pinjam`,`d`.`tgl_kembali` AS `tgl_kembali` from ((((`pegawai` `a` join `detail_pinjam` `b`) join `inventaris` `c`) join `peminjaman` `d`) join `ruang` `e`) where ((`e`.`id_ruang` = `c`.`id_ruang`) and (`a`.`id_pegawai` = `d`.`id_pegawai`) and (`b`.`id_inventaris` = `c`.`id_inventaris`) and (`b`.`id_peminjaman` = `d`.`id_peminjaman`)) order by `d`.`id_peminjaman` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  ADD PRIMARY KEY (`id_detail_pinjam`),
  ADD KEY `id_peminjaman` (`id_peminjaman`);

--
-- Indexes for table `inventaris`
--
ALTER TABLE `inventaris`
  ADD PRIMARY KEY (`id_inventaris`),
  ADD KEY `id_petugas` (`id_petugas`),
  ADD KEY `id_ruang` (`id_ruang`),
  ADD KEY `id_jenis` (`id_jenis`);

--
-- Indexes for table `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`),
  ADD KEY `id_pegawai` (`id_pegawai`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `id_level` (`id_level`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`id_ruang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pinjam`
--
ALTER TABLE `detail_pinjam`
  MODIFY `id_detail_pinjam` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `inventaris`
--
ALTER TABLE `inventaris`
  MODIFY `id_inventaris` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id_petugas` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `id_ruang` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
